import './vendor';

import { MovieDetailsPage } from './pages/movie-details';
import { MoviesListPage } from './pages/movies-list';
import { ROUTES } from './routes';
import { RouteService } from './services/router';

const root = document.getElementById('root');

RouteService.init()
  .on({
    route: ROUTES.HOME.path,
    run: () => {
      root.innerHTML = MoviesListPage();
    },
  })
  .on({
    route: ROUTES.MOVIE_DETAILS.path,
    run: ({ id }) => {
      root.innerHTML = MovieDetailsPage({ id });
    },
  })
  .start();
