import styles from './AppLayout.module.css';

export function AppLayout({ header, menu, content }) {
  return `
    ${header}
    <div class=${styles.container}>
      ${menu}
      ${content}
    </div>

  `;
}
