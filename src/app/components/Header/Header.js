import '../../zeroing.css';
import '../main.css';

import appLogo from '../../../assets/images/app-logo.svg';
import arrowDown from '../../../assets/images/arrow-down.svg';
import userAvatar from '../../../assets/images/avatar.jpg';
import searchFilter from '../../../assets/images/search-filter.svg';
import styles from './Header.module.css';

export function Header() {
  return `
    <header>
      <div class=${styles.container}>
        <a href="#"><img src=${appLogo} alt="Pixema"></a>

        <div class=${styles.search}>
          <input type="text" placeholder="Search" class=${styles.search_bar}>
          <a href="#" class=${styles.search_filter}><img src=${searchFilter} alt="search"></a>
        </div>

        <a href="#" class=${styles.user_bar}>
          <div class=${styles.user_avatar}>
            <img src=${userAvatar} alt="user avatar">
          </div>
          <p class=${styles.user_name}>Artem Lapitsky</p>
          <img src=${arrowDown} class=${styles.arrow_down}>
        </a>

    </header>
  `;
}
