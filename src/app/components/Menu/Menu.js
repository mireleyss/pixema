// import '../../zeroing.css';

// import '../main.css';
import favorites from '../../../assets/images/menu/favorites.svg';
import home from '../../../assets/images/menu/home.svg';
import settings from '../../../assets/images/menu/settings.svg';
import trends from '../../../assets/images/menu/trends.svg';
import styles from './Menu.module.css';

export function Menu() {
  return `
    <nav class=${styles.navigation}>
      <div class=${styles.container}>
        <ul>
          <li>
            <img src=${home}>
            <a href="#">Home</a>
          </li>
          <li>
            <img src=${trends}>
            <a href="#">Trends</a>
          </li>
          <li>
            <img src=${favorites}>
            <a href="#">Favourites</a>
          </li>
          <li>
            <img src=${settings}>
            <a href="#">Settings</a>
          </li>
        </ul>
      </div>
    </nav>
  `;
}
