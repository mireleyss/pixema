import styles from './MovieDetails.module.css';

const MovieInfo = (key, value) => {
  return `
    <div class=${styles.desc_info}>
      <p class=${styles.desc_info_name}>${key}</p>
      <p class=${styles.desc_info_value}>${value}</p>
    </div>
  `;
};

export const MovieDetails = (props) => {
  if (!props || typeof props !== 'object') {
    return '';
  }

  const { Poster, Title, Plot } = props;

  const info = (({
    Year,
    Released,
    BoxOffice,
    Country,
    Production,
    Actors,
    Director,
    Writer,
  }) => ({
    Year,
    Released,
    BoxOffice,
    Country,
    Production,
    Actors,
    Director,
    Writer,
  }))(props);

  return `
    <section class="${styles.desc}">
      <div class="${styles.container}">
        <img src="${Poster}" class="${styles.banner}"/>
        <div class="${styles.desc_text}">
          <h1 class="${styles.desc_title}">${Title}</h1>
          <p class="${styles.desc_plot}">${Plot}</p>
          <div>
            ${Object.entries(info)
              .map(([key, value]) => MovieInfo(key, value))
              .join('')}
          </div>
        </div>
      </div>
    </section>
  `;
};
