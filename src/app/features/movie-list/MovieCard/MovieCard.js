import styles from './MovieCard.module.css';

export const Item = (props) => {
  if (!props || typeof props !== 'object') {
    return '';
  }

  const { Title, imdbID, Poster } = props;

  return `
    <div class=${styles.movie_wrapper} data-movie-id=${imdbID}>
      <img src=${Poster} class=${styles.movie_banner} alt="${Title}">
      <span class="${`${styles.sub_1} ${styles.movie_title}`}">${Title}</span>
    </div>
  `;
};
