import { ROUTES } from '../../../routes';
import { RouteService } from '../../../services/router';
import styles from './MoviesList.module.css';
import { Item } from '../MovieCard'

const MOVIES_LIST_ELEMENT_ID = 'movies_list';

const getListItemId = (element) =>
  element.closest('[data-movie-id]')?.dataset.movieId;


let hasClickHandler = false;
const addListClickHandler = () => {
  const listElement = document.getElementById(MOVIES_LIST_ELEMENT_ID);

  if(!listElement) {
    hasClickHandler = false;
    return;
  }

  if (hasClickHandler) {
    return;
  }

  listElement?.addEventListener('click', (event) => {
    const id = getListItemId(event.target);

    if (!id) {
      return;
    }

    RouteService.push(ROUTES.MOVIE_DETAILS.buildPath({ id }));
  });
  hasClickHandler = true;
};

window.addEventListener('DOMContentLoaded', addListClickHandler);

const observer = new MutationObserver(() => {
  addListClickHandler();
});

observer.observe(document, {
  attributes: false,
  childList: true,
  characterData: false,
  subtree: true
});

export const MoviesList = ({ items } = {}) => {
  if (!items) {
    return '';
  }

  return `
    <div class=${styles.catalog}>
      <div class=${styles.container} id=${MOVIES_LIST_ELEMENT_ID}>
        ${items.map(Item).join('')}
      </div>
    </div>
  `;
}
