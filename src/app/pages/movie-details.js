import { mockDetailsMap } from '../../../mockData';
import { AppLayout } from '../components/AppLayout';
import { Header } from '../components/Header';
import { Menu } from '../components/Menu';
import { MovieDetails } from '../features/movie-details';

export function MovieDetailsPage({ id }) {
  const details = mockDetailsMap[id];

  if (!details) {
    return '';
  }

  return AppLayout({
    header: Header(),
    menu: Menu(),
    content: MovieDetails(details),
  });
}
