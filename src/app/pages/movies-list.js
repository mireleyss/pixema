import { mockList } from '../../../mockData';
import { AppLayout } from '../components/AppLayout';
import { Header } from '../components/Header';
import { Menu } from '../components/Menu';
import { MoviesList } from '../features/movie-list/MoviesList';

export function MoviesListPage() {
  return AppLayout({
    header: Header(),
    menu: Menu(),
    content: MoviesList({ items: mockList }),
  });
}
