export const ROUTES = {
  HOME: {
    path: '/',
  },
  MOVIE_DETAILS: {
    path: '/movie',
    buildPath: ({ id } = {}) => {
      const url = new URL(window.location.href);
      url.pathname = '/movie';
      if (id) {
        url.searchParams.set('id', id);
      }

      return url.pathname + url.search;
    },
  },
};
